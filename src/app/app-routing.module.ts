import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageInputComponent } from './page-input/page-input.component';
import { PageReviewsComponent } from './page-reviews/page-reviews.component';


const routes: Routes = [

  {
    path: 'page-input.component',
    component: PageInputComponent
  },
  {
    path: 'page-reviews.component',
    component: PageReviewsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
